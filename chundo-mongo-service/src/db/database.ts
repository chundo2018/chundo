import * as mongodb from 'mongodb';
import assert = require('assert');
var Q = require("q");

export class MongoDataAccessor {
    static mongoUrl: string = 'mongodb://127.0.0.1:27017/chundo';
    dbConnection: mongodb.MongoClient | null = null;
    db: mongodb.Db | null = null;
    
    public openConnection(){
        if(this.dbConnection == null){
            mongodb.MongoClient.connect(MongoDataAccessor.mongoUrl, (err:mongodb.MongoError, dbConnection) => {
                assert.equal(null, err);
                console.log("Connected correctly to MongoDB Server. (" + MongoDataAccessor.mongoUrl + ")");
                this.dbConnection = dbConnection;
                this.db = this.dbConnection.db("chundo");
            });
        }
    }

     // Close the existing connection.
    public closeDbConnection() {
        if (this.dbConnection) {
            this.dbConnection.close();
            this.dbConnection = null;
        }
    }

    public findAll<T>(collectionName: string, query: mongodb.FilterQuery<T> | null, cbf:(err:mongodb.MongoError | null, result:mongodb.Cursor<T> | null) => void){
        if(this.db != null){
            this.db.collection<T>(collectionName, (err:mongodb.MongoError, collection:mongodb.Collection) => {
                if(err) return cbf(err, null);
                if(query != null){
                    return cbf(null, collection.find(query));
                }else{
                    return cbf(null, collection.find({}));
                }
            });
        }
    }

    public saveOne<T>(collectionName: string, item: T, cbf:(err:mongodb.MongoError | null, insertedId: T | null) => void){
        if(this.db != null){
            this.db.collection(collectionName, (err:mongodb.MongoError, collection: mongodb.Collection) => {
                if(err) return cbf(err, null);
                collection.insertOne(item, (err, insertedId) => {
                    if(err) return cbf(err, null);
                    return cbf(null, item);
                });
            });
        }
    }

    public updateOne<T>(collectionName: string, query: mongodb.FilterQuery<T> | null, item: T, cbf:(err:mongodb.MongoError | null, result: T | null) => void){
        if(this.db != null){
            this.db.collection(collectionName, (err:mongodb.MongoError, collection: mongodb.Collection) => {
                if(err) return cbf(err, null);
                if(query != null){
                    collection.update(query, item, (err, upsertedId) => {
                        if(err) return cbf(err, null);
                        return cbf(null, item);
                    });
                }else{
                    let err:mongodb.MongoError = new mongodb.MongoError("'id' query is required for update.");
                    return cbf(err, null);
                }
            });
        }
    }


    public removeOne<T>(collectionName: string, item: T, cbf:(err:mongodb.MongoError | null, result: any) => void){
        if(this.db != null){
            this.db.collection(collectionName, (err:mongodb.MongoError, collection: mongodb.Collection) => {
                if(err) return cbf(err, null);
                collection.remove(item, (err, removeResult) => {
                    if(err) return cbf(err, null);
                    return cbf(null, removeResult.result);
                });
            });
        }
    }

    public findOne<T>(collectionName: string, query: mongodb.FilterQuery<T>, cbf:(err:mongodb.MongoError | null, result:T | null) => void){
        if(this.db != null){
            this.db.collection(collectionName, (err:mongodb.MongoError, collection:mongodb.Collection) => {
                if(err) return cbf(err, null);
                collection.findOne<T>(query, (err, result) => {
                    if(err) return cbf(err, null);
                    return cbf(null, result);
                });
            });
        }
    }

    public findResponse<T>(collectionName: string, query: mongodb.FilterQuery<T>, fieldOpt: mongodb.FindOneOptions, cbf:(err:mongodb.MongoError | null, result:T | null) => void){
        if(this.db != null){
            this.db.collection(collectionName, (err:mongodb.MongoError, collection:mongodb.Collection) => {
                if(err) return cbf(err, null);
                collection.findOne<T>(query, fieldOpt, (err, result) => {
                    if(err) return cbf(err, null);
                    return cbf(null, result);
                });
            });
        }
    }
}