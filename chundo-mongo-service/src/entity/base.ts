import * as express from "express";
import { MongoDataAccessor } from "../db/database";
export class ChundoBaseController {
  dataAccessor: MongoDataAccessor;
  constructor(dataAccessor: MongoDataAccessor) {
    this.dataAccessor = dataAccessor;
  }

  // Send an error message.
  public sendErrorMessage(res: express.Response, e?: Error) {
    if (e) {
      let ex: string = JSON.stringify(e);
      return res.status(500).json({ Message: e.message, ExceptionMessage: ex });
    } else {
      res.sendStatus(500);
    }
  }
}
