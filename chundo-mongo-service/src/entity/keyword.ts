import { DSMessage } from "./ds_message";
import { ObjectID } from "bson";
import { ChundoBaseController } from './base';
import { MongoDataAccessor } from '../db/database';
import { Dongsuk, IDongsuk } from './dongsuk';
import express = require("express");

export interface IKeyword {
    keywordId: string;
    key: string[];
    response: DSMessage[];
}

export class Keyword implements IKeyword {
    public keywordId: string;
    public key: string[];
    public response: DSMessage[];

    public constructor(keyword?: Keyword) {
        if (keyword != null) {
            if (keyword.keywordId == null) {
                this.keywordId = new ObjectID().toHexString();
            } else {
                this.keywordId = keyword.keywordId;
            }
            this.key = keyword.key;
            this.response = keyword.response;
        } else {
            this.keywordId = new ObjectID().toHexString();
            this.key = [];
            this.response = [];
        }
    }
}

export class KeywordController extends ChundoBaseController {

    constructor(app: express.Express, dataAccessor: MongoDataAccessor) {
        super(dataAccessor);
        app.post("/dongsuk/keyword/save", this.postSaveKeyword()); //동석
        app.post("/dongsuk/keyword/delete", this.postDeleteKeyword()); //roomid, keywordid
    }
    public postSaveKeyword(): any {
        return (req: express.Request, res: express.Response) => {
            let dongsuk: Dongsuk = new Dongsuk(<IDongsuk>req.body);
            if (dongsuk != null) {
                console.log("[Request] Save Keyword. [ROOMID: " + dongsuk.roomId +"]");
                this.dataAccessor.findOne<Dongsuk>(
                    Dongsuk.COLLECTION,
                    { roomId: dongsuk.roomId },
                    (err, result) => {
                        let inputKeywords: Keyword[] = dongsuk.keyword;
                        inputKeywords.forEach(elem => {
                            if (elem.keywordId == null) {
                                elem.keywordId = new ObjectID().toHexString();
                            }
                        });

                        if (result == null) {
                            result = dongsuk;
                            this.dataAccessor.saveOne(Dongsuk.COLLECTION, result, (err, result) => {
                                if (err) return this.sendErrorMessage(res, err);
                                if (result) {
                                    console.log("[SUCCESS] Saved keyword. [ROOMID: " + result.roomId +"]");
                                    res.json(result);
                                }
                            });
                        } else {
                            if (dongsuk.keyword.length > 0) {
                                let isModified: boolean = false;
                                for (let i = 0; i < result.keyword.length; i++) {
                                    let keyword: Keyword = result.keyword[i];
                                    if (keyword.keywordId == dongsuk.keyword[0].keywordId) {
                                        keyword.key = dongsuk.keyword[0].key;
                                        keyword.response = dongsuk.keyword[0].response;
                                        isModified = true;
                                        break;
                                    }
                                }

                                if (!isModified) {
                                    result.keyword.push(dongsuk.keyword[0]);
                                }
                            }

                            this.dataAccessor.updateOne(Dongsuk.COLLECTION, { roomId: result.roomId }, result, (err, result) => {
                                if (err) return this.sendErrorMessage(res, err);
                                if (result) {
                                    console.log("[SUCCESS] Saved keyword. [ROOMID: " + result.roomId +"]");
                                    res.json(result);
                                }
                            });
                        }
                    });
            } else {
                console.log("[ERROR] Save keyword failed. Because the dongsuk json data is required.");
                let err: Error = new Error("The dongsuk json data is required.");
                this.sendErrorMessage(res, err);
            }
        };
    }

    public postDeleteKeyword(): any {
        return (req: express.Request, res: express.Response) => {
            let roomId: string = req.body.roomId;
            let keywordId: string = req.body.keywordId;
            if (roomId != null && roomId.length > 0) {
                if(keywordId != null && keywordId.length > 0){
                    console.log("[Request] Delete keyword. [ROOMID: " + roomId +", KEYWORDID:"+ keywordId +"]");
                    this.dataAccessor.findOne<Dongsuk>(
                        Dongsuk.COLLECTION,{ roomId: roomId }, 
                        (err, result) => {
                            if(result == null){
                                console.log("[ERROR] Delete keyword failed. Because the dongsuk is not found.");
                                let err: Error = new Error("{roomId:"+ roomId +"} dongsuk is empty.");
                                this.sendErrorMessage(res, err);
                            }else{
                                let filterKeyword:Keyword[] = result.keyword.filter(val =>{
                                   return val.keywordId != keywordId;
                                }); 
                                result.keyword = filterKeyword;
                                this.dataAccessor.updateOne(Dongsuk.COLLECTION, { roomId: result.roomId }, result, (err, result) => {
                                    if (err) return this.sendErrorMessage(res, err);
                                    if (result) {
                                        console.log("[SUCCESS] Saved keyword. [ROOMID: " + result.roomId +"]");
                                        res.json(result);
                                    }
                                });
                            }
                        });
                }else{
                    console.log("[ERROR] Delete keyword failed. Because 'keywordId' is empty.");
                    let err: Error = new Error("'keywordId' is required.");
                    this.sendErrorMessage(res, err);
                }
            } else {
                console.log("[ERROR] Delete keyword failed. Because 'roomId' is empty.");
                let err: Error = new Error("'roomId' is required.");
                this.sendErrorMessage(res, err);
            }
        }
    }
}