import * as mongodb from "mongodb";
import { MongoDataAccessor } from "../db/database";
import { ChundoBaseController } from "./base";
import { DSMessage } from './ds_message';
import { Keyword, IKeyword } from './keyword';
import { ObjectID } from 'bson';
import express = require("express");

export interface IDongsuk {
  roomId: string;
  keyword: Keyword[];
}

export class Dongsuk implements IDongsuk {
  public static readonly COLLECTION: string = "Dongsuk";

  public roomId: string;
  public keyword: Keyword[];
  public constructor(dongsuk?: IDongsuk) {
    if (dongsuk != null) {
      this.roomId = dongsuk.roomId;
      this.keyword = dongsuk.keyword;
    } else {
      this.roomId = "";
      this.keyword = [];
    }
  }
}

export class DongsukController extends ChundoBaseController {
  public static readonly DONGSUK: string = "Dongsuk";
  constructor(app: express.Express, dataAccessor: MongoDataAccessor) {
    super(dataAccessor);
    app.get("/dongsuk/find", this.find());
    app.get("/dongsuk/save", this.getSaveDongsuk());
    app.post("/dongsuk/save", this.postSaveDongsuk());
    app.get("/dongsuk/delete", this.delete());
    app.get("/dongsuk/response", this.response());
  }

  public find(): any {
    return (req: express.Request, res: express.Response) => {
      let roomId: string = req.query.id;
      if (roomId != null && roomId.length > 0) {
        console.log("[Request] Find Dongsuk. [ROOMID: " + roomId + "]");
        this.dataAccessor.findOne<Dongsuk>(
          Dongsuk.COLLECTION,
          { roomId: roomId },
          (err, result) => {
            if (err) return this.sendErrorMessage(res, err);
            if (result) {
              console.log("[SUCCESS] Find Dongsuk. [ROOMID: " + result.roomId +"]");
              res.json(result.keyword);
            } else {
              console.log("[ERROR] Delete keyword failed. Because room is not found by id. (" + roomId + ")");
              let err: Error = new Error("Not found the room by id. (" + roomId + ")");
              this.sendErrorMessage(res, err);
            }
          }
        );
      } else {
        console.log("[Request] Find all Dougsuk.");
        this.dataAccessor.findAll<Dongsuk>(Dongsuk.COLLECTION, null, (err, result) => {
          if (err) return this.sendErrorMessage(res, err);
          if (result) {
            result.toArray((err: mongodb.MongoError, dongsuks: Dongsuk[]) => {
              res.json(dongsuks);
            });
          }
        });
      }
    };
  }

  public getSaveDongsuk(): any {
    return (req: express.Request, res: express.Response) => {
      let roomId: string = req.query.id;
      if (roomId != null && roomId.length > 0) {
        this.dataAccessor.findOne<Dongsuk>(
          Dongsuk.COLLECTION,
          { roomId: roomId },
          (err, result) => {

            let keys: string[] = ["asdf", "zxcv"];
            let dsMsg: DSMessage[] = [new DSMessage("1234"), new DSMessage("56768")];
            let keyword: Keyword = new Keyword();
            keyword.key = keys;
            keyword.response = dsMsg;

            let keys1: string[] = ["test", "test1"];
            let dsMsg1: DSMessage[] = [new DSMessage("4567"), new DSMessage("9876")];
            let keyword1: Keyword = new Keyword();
            keyword1.key = keys1;
            keyword1.response = dsMsg1;

            if (result == null) {
              result = new Dongsuk();
              result.roomId = roomId;
              result.keyword = [keyword, keyword1];
              this.dataAccessor.saveOne(Dongsuk.COLLECTION, result, (err, result) => {
                if (err) return this.sendErrorMessage(res, err);
                if (result) {
                  res.json(result);
                }
              });
            } else {
              result.keyword = [keyword, keyword1];
              this.dataAccessor.updateOne(Dongsuk.COLLECTION, { roomId: result.roomId }, result, (err, result) => {
                if (err) return this.sendErrorMessage(res, err);
                if (result) {
                  res.json(result);
                }
              });
            }
          }
        );
      } else {
        let err: Error = new Error("'id' is required.");
        this.sendErrorMessage(res, err);
      }
    };
  }

  public postSaveDongsuk(): any {
    return (req: express.Request, res: express.Response) => {
      let dongsuk: Dongsuk = new Dongsuk(<IDongsuk>req.body);
      if (dongsuk != null) {
        this.dataAccessor.findOne<Dongsuk>(
          Dongsuk.COLLECTION,
          { roomId: dongsuk.roomId },
          (err, result) => {
            let inputKeywords: Keyword[] = dongsuk.keyword;
            inputKeywords.forEach(elem => {
              if (elem.keywordId == null) {
                elem.keywordId = new ObjectID().toHexString();
              }
            });

            if (result == null) {
              result = dongsuk;
              this.dataAccessor.saveOne(Dongsuk.COLLECTION, result, (err, result) => {
                if (err) return this.sendErrorMessage(res, err);
                if (result) {
                  res.json(result);
                }
              });
            } else {
              result.keyword = dongsuk.keyword;
              this.dataAccessor.updateOne(Dongsuk.COLLECTION, { roomId: result.roomId }, result, (err, result) => {
                if (err) return this.sendErrorMessage(res, err);
                if (result) {
                  res.json(result);
                }
              });
            }
          });
      } else {
        let err: Error = new Error("The dongsuk json data is required.");
        this.sendErrorMessage(res, err);
      }
    };
  }

  public delete(): any {
    return (req: express.Request, res: express.Response) => {
      let roomId: string = req.query.id;
      if (roomId != null && roomId.length > 0) {
        this.dataAccessor.removeOne(Dongsuk.COLLECTION, { roomId: roomId }, (err, result) => {
          if (err) return this.sendErrorMessage(res, err);
          if (result) {
            res.json(result);
          }
        });
      } else {
        let err: Error = new Error("'id' is required.");
        this.sendErrorMessage(res, err);
      }
    };
  }

  public response(): any {
    return (req: express.Request, res: express.Response) => {
      let roomId: string = req.query.id;
      let keyword: string = req.query.keyword;
      if (roomId != null && roomId.length > 0) {
        if (keyword != null && keyword.length > 0) {
          keyword = decodeURI(keyword);
          let query: mongodb.FilterQuery<Dongsuk> = { $and: [{ "roomId": roomId }, { "keyword.key": keyword }] };
          let fieldOpt: mongodb.FindOneOptions = { fields: { "keyword.$": 1 } };
          this.dataAccessor.findResponse(Dongsuk.COLLECTION, query, fieldOpt, (err, result) => {
            if (err) return this.sendErrorMessage(res, err);
            if (result) {
              let dongsuk: Dongsuk = new Dongsuk(<IDongsuk>result);
              res.send(this.randomResponse(dongsuk));
            } else {
              res.send(new DSMessage(""));
            }
          });
        } else {
          let err: Error = new Error("'keyword' is required.");
          this.sendErrorMessage(res, err);
        }
      } else {
        let err: Error = new Error("'id' is required.");
        this.sendErrorMessage(res, err);
      }
    };
  }

  private randomResponse(dongsuk: Dongsuk): DSMessage | null {
    if (dongsuk.keyword.length > 0) {
      let keyword: Keyword = dongsuk.keyword[0];
      if (keyword.response.length > 0) {
        let max: number = keyword.response.length - 1;
        return keyword.response[this.random(0, max)];
      } else {
        return new DSMessage("");
      }
    } else {
      return new DSMessage("");
    }
  }

  private random(n1: number, n2: number) {
    return Math.floor((Math.random() * (n2 - n1 + 1)) + n1);
  }
}
