export interface IDSMessage{
    message: string;
    weight: number;
}

export class DSMessage implements IDSMessage{
    public message: string;
    public weight: number;

    public constructor(message: string) {
        this.message = message;
        this.weight = 0;
    }
}