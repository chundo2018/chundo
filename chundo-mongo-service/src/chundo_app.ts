import { MongoDataAccessor } from './db/database';
import { DongsukController } from './entity/dongsuk';
import { KeywordController } from './entity/keyword';
import express = require('express');
import path = require('path');
import bodyParser = require('body-parser');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(path.join(__dirname, 'chundo-web')));

let mongoDataAccessor:MongoDataAccessor = new MongoDataAccessor();
mongoDataAccessor.openConnection();
let dongsukController:DongsukController = new DongsukController(app, mongoDataAccessor);
let keywordController:KeywordController = new KeywordController(app, mongoDataAccessor);

app.use(function(error:Error, req: express.Request, res: express.Response, next: express.NextFunction) {
  dongsukController.sendErrorMessage(res, error);
});

app.get('/*', (req: express.Request, res: express.Response) => {
  res.sendFile(path.join(__dirname, 'chundo-web/index.html'));
});

app.listen(80, () => {
  console.log('Example app listening on port 80!');
});

