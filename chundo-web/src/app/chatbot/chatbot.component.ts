import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Keyword } from '../keyword/keyword';
import { KeywordService } from '../keyword/keyword.service';

@Component({
  selector: 'app-chatbot',
  templateUrl: './chatbot.component.html',
  styleUrls: ['./chatbot.component.css'],
  providers: [KeywordService]

})
export class ChatbotComponent implements OnInit {

  title = 'app';
  keywords: Keyword[] = [];
  roomId: string;

  constructor(private route: ActivatedRoute, private keywordkService: KeywordService) {
  }

  ngOnInit() {
    this.roomId = this.route.snapshot.paramMap.get('roomid');
    console.log(this.roomId);
    this.keywordkService.getKeywords(this.roomId).subscribe(result => {

      this.keywords = result;

      if (this.keywords.length === 0) {
        this.addEmptyKeyword();
      }

    });
  }

  addEmptyKeyword() {
    console.log(ChatbotComponent.name + '.addEmptyKeyword()');
    // 첫 인덱스에 빈 키워드 추가
    this.keywords.push({ keywordId: null, key: [], response: [] });

  }

  formEvent(event: { type: 'delete' | 'save', target: Keyword }) {

    if (event.type === 'delete') {

      this.keywordkService.deleteKeyword(this.roomId, event.target.keywordId).subscribe(
        () => {
          this.keywords = this.keywords.filter(data => data !== event.target);
        }
      );
    }

    this.keywordkService.getKeywords(this.roomId).subscribe(
      result => this.keywords = result
    );
  }
}
