import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from '../app.component';
import { ChatbotComponent } from '../chatbot/chatbot.component';

const routes: Routes = [
  { path: '', redirectTo: '/sample', pathMatch: 'full' },
  { path: ':roomid', component: ChatbotComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
