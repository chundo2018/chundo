import { DSMessage } from './ds_message';

// export interface IKeyword{
//     key: string[];
//     response: DSMessage[];
// }

export interface Keyword {
    keywordId: string,
    key: string[];
    response: DSMessage[];
}
