import { Observable } from 'rxjs';
import { IDongsuk } from './dongsuk';
import { Keyword } from './keyword';

export interface IkeywordService {

    getKeywords(roomId: string): Observable<Keyword[]>;
    saveKeyword(dongsuk: IDongsuk): Observable<Keyword>;
    deleteKeyword(roomId: String, keywordId: String): Observable<{roomId: string, keywordId: string}>;
    
}
