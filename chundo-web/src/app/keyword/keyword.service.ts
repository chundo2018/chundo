import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { IkeywordService } from './ikeyword-service';
import { Keyword } from './keyword';
import { IDongsuk } from './dongsuk';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable()
export class KeywordService implements IkeywordService {
  private url = '/dongsuk';  // URL to web api
  
  constructor(private http: HttpClient) { }
  
  getKeywords(roomId: string): Observable<Keyword[]> {
    return this.http.get<Keyword[]>(`${this.url}/find?id=${roomId}`).pipe(
      tap(keyword => this.log(`fetched keyword`)),
      catchError(this.handleError('getKeywords', []))
    );
  }
  
  saveKeyword(dongsuk: IDongsuk): Observable<Keyword> {
    console.log(`${KeywordService.name}.savekeywords() : ${dongsuk.roomId}`);
    return this.http.post<Keyword>(`${this.url}/keyword/save`, dongsuk, httpOptions);
  }
  
  deleteKeyword(roomId: string, keywordId: string): Observable<{roomId: string, keywordId: string}> {
    return this.http.post<{roomId: string, keywordId: string}>(`${this.url}/keyword/delete`, {roomId: roomId, keywordId: keywordId}, httpOptions).pipe(
      tap(keywordId => this.log(`delete keyword: ${keywordId}`)),
      catchError(this.handleError(`deleteKeyword`, {roomId: roomId, keywordId: keywordId}))
    );
  }

  /**
 * Handle Http operation that failed.
 * Let the app continue.
 * @param operation - name of the operation that failed
 * @param result - optional value to return as the observable result
 */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a KeywordService message */
  private log(msg: String) {

  }
}
