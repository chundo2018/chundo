import { Keyword } from './keyword';

export interface IDongsuk {
  roomId: string;
  keyword: Keyword[];
}

export class Dongsuk implements IDongsuk {

  public roomId: string;
  public keyword: Keyword[];

  public constructor(roomId: string, keyword: Keyword[]) {
    this.roomId = roomId;
    this.keyword = keyword;
  }
}
