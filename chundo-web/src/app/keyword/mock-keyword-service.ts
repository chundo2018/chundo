import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { DSMessage } from './ds_message';
import { IkeywordService } from './ikeyword-service';
import { Keyword } from './keyword';
import { IDongsuk } from './dongsuk';

@Injectable()
export class MockKeywordService implements IkeywordService {
    
    getKeywords(roomId: string): Observable<Keyword[]> {

        const keywords: Keyword[] = [];
        keywords.push(this.generateKeyword(['안녕'], [{ message: 'hi', weight: 0 }, { message: 'hello', weight: 0 }]));
        keywords.push(this.generateKeyword(['정성일'], [{ message: '메롱', weight: 0 }]));
        keywords.push(this.generateKeyword(['조욱재'], [{ message: '뭐', weight: 0 }]));
        keywords.push(this.generateKeyword(['박무군', '김시완'], [{ message: '안녕', weight: 0 }, { message: '잉?', weight: 0 }]));
        keywords.push(this.generateKeyword(['곽충석'], [{ message: 'ㅋㅋㅋㅋ', weight: 0 }, { message: 'ㅎㅎㅎㅎ', weight: 0 }]));

        return of(keywords);
    }

    private generateKeyword(mockKeys: string[], mockMsgs: DSMessage[]): Keyword {
        const mockKeyword: Keyword = {keywordId:`test`, key: mockKeys, response: mockMsgs };

        return mockKeyword;
    }

    saveKeyword(dongsuk: IDongsuk): Observable<Keyword> {
        throw new Error("Method not implemented.");
    }
    
    deleteKeyword(roomId: String, keywordId: String): Observable<{roomId: string, keywordId: string}> {
        throw new Error("Method not implemented.");
    }
}
