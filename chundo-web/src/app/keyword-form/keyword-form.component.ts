import { AfterViewChecked, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { from } from 'rxjs';
import { map } from 'rxjs/operators';
import { Keyword } from '../keyword/keyword';
import { KeywordService } from '../keyword/keyword.service';

@Component({
  selector: 'app-keyword-form',
  templateUrl: './keyword-form.component.html',
  styleUrls: ['./keyword-form.component.css'],

})
export class KeywordFormComponent implements OnInit, AfterViewChecked {
  @Input() keyword: Keyword;

  @Input() roomId: string;

  @Input() isShow: boolean;

  @Output() formEvent: EventEmitter<{ type: 'delete' | 'save', target: Keyword }> = new EventEmitter();

  strKeys: string[] = [];
  strMessages: string[] = [];

  constructor(private modalService: NgbModal, private keywordservice: KeywordService) { }

  ngOnInit() {
    // 멤버 초기화
    this.strKeys = this.keyword.key;
    from(this.keyword.response)
      .pipe(
        map(dsMsg => {
          return dsMsg.message;
        })
      ).subscribe(result => this.strMessages.push(result));
  }

  ngAfterViewChecked() { }

  save() {
    let keyword;
    let dsMessages = [];
    from(this.strMessages).pipe(
      map(msg => {
        return { message: msg.trim(), weight: 0 }
      })
    ).subscribe(dsMsg => {

      if(dsMsg.message.length !== 0){
        dsMessages.push(dsMsg)
      }
    });
    
    keyword = { keywordId: this.keyword.keywordId, key: this.strKeys.map(key => key.trim()).filter(key => key.length !== 0), response: dsMessages };
    this.saveKeyword(keyword);
  }

  private saveKeyword(keyword: Keyword) {
    this.keywordservice.saveKeyword({ roomId: this.roomId, keyword: [keyword] }).subscribe(
      () => {
        // Update isShow
        this.isShow = false;
        this.keyword = keyword;
        this.formEvent.emit({ type: 'save', target: this.keyword });
      });
  }

  delete() {
    this.formEvent.emit({ type: 'delete', target: this.keyword });
  }

  saveKeys(event: any): void {
    this.isShow = true;
    this.strKeys = event.target.value.toString().split(`,`);
  }

  saveMessages(event: any): void {

    this.isShow = true;
    this.strMessages = event.target.value.toString().split(`,`);
  }

  openDeleteModal(content) {
    // Open delete keyword modal.
    this.modalService.open(content);
  }
}
