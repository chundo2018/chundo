import express = require('express');
const request = require('supertest');
import app = require('../src/message_app');
const crypto = require('crypto');

const channelSecret = '05334ab5c12c1bc6e71dcbcfe9b85117'; // Channel secret string

const createSignature = (body: any) => {
  return crypto
    .createHmac('SHA256', channelSecret)
    .update(JSON.stringify(body))
    .digest('base64');
};

describe('Server POST /webhook', function() {
  it('alarm create', function(done) {
    const ent = {
      events: [
        {
          replyToken: '05334ab5c12c1bc6e71dcbcfe9b85117',
          type: 'message',
          timestamp: 1462629479859,
          source: {
            type: 'user',
            userId: 'U206d25c2ea6bd87c17655609a1c37cb8'
          },
          message: {
            id: '325708',
            type: 'text',
            text: 'sdfds'
          }
        }
      ]
    };
    request(app)
      .post('/webhook')
      .set('X-Line-Signature', createSignature(ent))
      .send(ent)
      .expect(200, done);
  });

  // request.post({
  //   url: webhook.url,
  //   headers: {
  //     'X-Line-Signature': createSignature(webhook.secret, body),
  //     'Content-Type': 'application/json'
  //   },
  //   body: body
  // }, (e, r, body) => res.json({
  //   status: r.statusCode,
  //   body: body,
  // }));
  // it('alarm query', function(done) {
  //   request(app)
  //     .post('/alarm')
  //     .send({
  //       desc: 'alarm query test',
  //       alarmName: 'create query',
  //       id: 'aaa'
  //     })
  //     .expect(200, done);
  // });

  // it('alarm wrong', function(done) {
  //   request(app)
  //     .post('/webhook')
  //     .send({
  //       events: [
  //         {
  //           replyToken: 'nHuyWiB7yP5Zw52FIkcQobQuGDXCTA',
  //           type: 'message',
  //           timestamp: 1462629479859,
  //           source: {
  //             type: 'user',
  //             userId: 'U206d25c2ea6bd87c17655609a1c37cb8'
  //           },
  //           message: {
  //             id: '325708',
  //             type: 'text',
  //             text: '@alam -c -t "* * * * * *" -n jw'
  //           }
  //         }
  //       ]
  //     })
  //     .expect(200, done);
  // });

  // it('alarm wrong2', function(done) {
  //   request(app)
  //     .post('/webhook')
  //     .send({
  //       events: [
  //         {
  //           replyToken: 'nHuyWiB7yP5Zw52FIkcQobQuGDXCTA',
  //           type: 'message',
  //           timestamp: 1462629479859,
  //           source: {
  //             type: 'user',
  //             userId: 'U206d25c2ea6bd87c17655609a1c37cb8'
  //           },
  //           message: {
  //             id: '325708',
  //             type: 'text',
  //             text: '@알람 -cs'
  //           }
  //         }
  //       ]
  //     })
  //     .expect(200, done);
  // });

  // it('alarm list', function(done) {
  //   request(app)
  //     .post('/webhook')
  //     .send({
  //       events: [
  //         {
  //           replyToken: 'nHuyWiB7yP5Zw52FIkcQobQuGDXCTA',
  //           type: 'message',
  //           timestamp: 1462629479859,
  //           source: {
  //             type: 'user',
  //             userId: 'U206d25c2ea6bd87c17655609a1c37cb8'
  //           },
  //           message: {
  //             id: '325708',
  //             type: 'text',
  //             text: '@alarm -list'
  //           }
  //         }
  //       ]
  //     })
  //     .expect(200, done);
  // });
  // it('alarm help', function(done) {
  //   request(app)
  //     .post('/webhook')
  //     .send({
  //       events: [
  //         {
  //           replyToken: 'nHuyWiB7yP5Zw52FIkcQobQuGDXCTA',
  //           type: 'message',
  //           timestamp: 1462629479859,
  //           source: {
  //             type: 'user',
  //             userId: 'U206d25c2ea6bd87c17655609a1c37cb8'
  //           },
  //           message: {
  //             id: '325708',
  //             type: 'text',
  //             text: '@alarm -h'
  //           }
  //         }
  //       ]
  //     })
  //     .expect(200, done);
  // });
});
