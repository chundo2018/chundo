import * as http from 'http';
import * as qs from 'querystring';
interface mm {
  message: string;
  weight: number;
}
function httpRequest(params: string) {
  return new Promise(function(resolve, reject) {
    const options = {
      host: 'malshan.tk',
      port: 3000,
      path: `/dongsuk/response?id=Ue9d73cdf0effb2f1fc3c3cb3d90ea2e3&keyword=${qs.escape('박무군')}`,
      headers: {
        'Accept-Charset': 'utf-8'
      },
      method: 'GET'
    };
    var req = http
      .get(options, res => {
        // reject on bad statu
        // cumulate data
        let body: string = '';
        res.on('data', function(chunk) {
          body += chunk;
        });
        // resolve on end
        res.on('end', function() {
          try {
            const message: string = JSON.parse(body)['message'];
            resolve(message);
          } catch (e) {
            reject(e);
          }
        });
      })
      .on('error', function(err) {
        reject(err);
      });
    // IMPORTANT
    req.end();
  });
}

function getResponseMessage(message: string, id: string) {
  return new Promise(function(resolve, reject) {
    const options = {
      host: 'malshan.tk',
      port: 3000,
      path: `/dongsuk/response?id=${id}&keyword=${qs.escape(message)}`,
      headers: {
        'Accept-Charset': 'utf-8'
      },
      method: 'GET'
    };
    const req = http
      .get(options, resp => {
        let data: string = '';
        resp.on('data', function(chunk) {
          data += chunk;
        });
        resp.on('end', function() {
          try {
            const message: string = JSON.parse(data)['message'];
            resolve(message);
          } catch (err) {
            console.log(err);
            reject(err);
          }
        });
      })
      .on('error', err => {
        console.log('Error: ' + err.message);
        reject(err);
      });
    req.end();
  });
}

async function hahahah() {
  let k = '';
  await getResponseMessage('정성일', 'Ue9d73cdf0effb2f1fc3c3cb3d90ea2e3')
    .then(body => {
      k = body.toString();
    })
    .catch(err => {
      console.log(err);
    });

  return k;
}

function kkk() {
  let hh = hahahah();
  hh.then(result => console.log(result));
}

kkk();
