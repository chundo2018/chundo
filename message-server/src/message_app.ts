import * as line from '@line/bot-sdk';
import * as express from 'express';
import * as fs from 'fs';
import * as http from 'http';
import * as https from 'https';
import * as qs from 'querystring';
import { CHANNEL_ACCESS_TOKEN, CHANNEL_SECRET } from './server.config';
import { ImageMessage } from '@line/bot-sdk';

const config = {
  channelAccessToken: CHANNEL_ACCESS_TOKEN,
  channelSecret: CHANNEL_SECRET
};

const options = {
  key: fs.readFileSync('/etc/letsencrypt/live/malshan.tk/privkey.pem'),
  cert: fs.readFileSync('/etc/letsencrypt/live/malshan.tk/fullchain.pem')
};

const port2 = 443;

const app = express();

app.use(express.urlencoded());

https.createServer(options, app).listen(port2, function() {
  console.log(`Https server listening on port ${port2}`);
});

app.post('/webhook', line.middleware(config), (req: express.Request, res: express.Response) => {
  Promise.all(req.body.events.map(handleEvent))
    .then(result => res.json(result))
    .catch(err => {
      res.status(500).end();
    });
});

const client = new line.Client(config);

async function handleEvent(event: line.MessageEvent) {
  if (event.type !== 'message' || event.message.type !== 'text') {
    return Promise.resolve(null);
  }
  let message = event.message.text;
  const keywordArray = ['응답하라', '마요미', '마요미 나와라', '우리 동석이', '하이 동석', '헤이 동석', '동석이 뿅뿅', '동석아 나랑 맞짱 뜰래', '마동석'];

  if (
    keywordArray.find(function(startupKeyword: string): boolean {
      return startupKeyword === message;
    })
  ) {
    event.message.text = `http://malshan.tk/${getSourceId(event.source)}\n\n응답 메시지 편집하러 가즈아~~~`;
    return client.replyMessage(event.replyToken, event.message);
  } else {
    await getResponseMessage(message, getSourceId(event.source)).then(responseMessage => {
      message = responseMessage.toString();
      Promise.resolve(message);
    });
    event.message.text = message;
    return client.replyMessage(event.replyToken, event.message);
  }
}

function isImageMessage(message: string): boolean {
  if (message.endsWith('.jpg') || message.endsWith('.jpeg')) {
    return true;
  }
  return false;
}

function getResponseMessage(message: string, id: string) {
  return new Promise(function(resolve, reject) {
    const options = {
      host: 'localhost',
      port: 80,
      path: `/dongsuk/response?id=${id}&keyword=${qs.escape(message)}`,
      headers: {
        'Accept-Charset': 'utf-8'
      },
      method: 'GET'
    };
    http
      .get(options, resp => {
        let data = '';
        resp.setEncoding('utf-8');
        resp.on('data', chunk => {
          data += chunk;
        });
        resp.on('end', () => {
          try {
            const message = JSON.parse(data)['message'];
            resolve(message);
          } catch (err) {
            console.log(err);
            reject(err);
          }
        });
      })
      .on('error', err => {
        console.log('Error: ' + err.message);
        reject(err);
      });
  });
}

function getSourceId(source: line.EventSource): string {
  let id: string = '';
  switch (source.type) {
    case 'user':
      id = source.userId;
      break;
    case 'group':
      id = source.groupId;
      break;
    case 'room':
      id = source.roomId;
      break;
    default:
      break;
  }
  return id;
}

export = app;
