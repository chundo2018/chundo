export const CHANNEL_ACCESS_TOKEN: string = '';
export const CHANNEL_SECRET: string = '';
export const SSL_CERT: string = '';
export const SSL_KEY: string = '';
